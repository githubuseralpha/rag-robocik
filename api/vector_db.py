import pathlib
import re

from langchain.embeddings import HuggingFaceEmbeddings
from langchain.vectorstores import Chroma
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.retrievers import ParentDocumentRetriever
from langchain.storage import InMemoryStore
from langchain.document_loaders import PyPDFium2Loader, Docx2txtLoader
from tqdm import tqdm


def load_model(modelPath, device="cpu"):
    model_kwargs = {"device": device}
    encode_kwargs = {"normalize_embeddings": True}

    embeddings = HuggingFaceEmbeddings(
        model_name=modelPath, model_kwargs=model_kwargs, encode_kwargs=encode_kwargs
    )
    return embeddings


def get_doc_loader(doc_path):
    suffix = pathlib.Path(doc_path).suffix
    if suffix == ".pdf":
        return PyPDFium2Loader(doc_path)
    elif suffix == ".docx":
        return Docx2txtLoader(doc_path)


def proccess_doc_content(doc):
    for page in doc:
        text = page.page_content.replace("\r", "")
        modified_text = re.sub(r"\n+", "\n", text)
        page.page_content = modified_text


def get_text_splitter(chunk_size=1000, chunk_overlap=150):
    return RecursiveCharacterTextSplitter(
        chunk_size=chunk_size, chunk_overlap=chunk_overlap
    )


def create_vector_store(embedding_model, data_path):
    documents = []

    print("Loading documents...")
    for doc_path in tqdm(pathlib.Path(data_path).glob("**/*")):
        if doc_path.is_file():
            try:
                loader = get_doc_loader(doc_path.as_posix())
                if loader is None:
                    continue
                doc = loader.load()
                proccess_doc_content(doc)
                documents.extend(doc)
            except Exception as e:
                print(f"Error loading {doc_path.as_posix()}: {e}")
                continue

    child_splitter = get_text_splitter(chunk_size=150, chunk_overlap=50)
    parent_splitter = get_text_splitter(chunk_size=1500, chunk_overlap=300)

    vector_store = Chroma(
        collection_name="split_parents", embedding_function=embedding_model
    )
    retriever = get_retriever(vector_store, child_splitter, parent_splitter)

    print("Adding documents to vector store...")
    retriever.add_documents(documents)
    return vector_store, retriever


def get_retriever(vector_store, child_splitter, parent_splitter):
    store = InMemoryStore()
    retriever = ParentDocumentRetriever(
        vectorstore=vector_store,
        docstore=store,
        child_splitter=child_splitter,
        parent_splitter=parent_splitter,
        search_kwargs={"k": 4},
    )
    return retriever
