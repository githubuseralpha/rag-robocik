import argparse

from langchain import hub
from langchain.chat_models import ChatOpenAI
from langchain.schema import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from dotenv import load_dotenv

from vector_db import load_model, create_vector_store, get_retriever

EMBEDDING_MODEL_NAME = "intfloat/multilingual-e5-large"


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--device", type=str, default="cpu", help="Device to run on (default: cpu)"
    )
    parser.add_argument(
        "--data_path",
        type=str,
        default="data/raw",
        help="Path to data (default: data/raw)",
    )
    parser.add_argument(
        "--dotenv_path",
        type=str,
        default=".env",
        help="Path to .env file (default: .env)",
    )
    return parser.parse_args()


def get_rag_chain(device="cpu", data_path="data/raw"):
    print("Loading embedding model...")
    embedding_model = load_model(EMBEDDING_MODEL_NAME, device=device)

    _, retriever = create_vector_store(embedding_model, data_path)

    prompt = hub.pull("rlm/rag-prompt")
    llm = ChatOpenAI(model_name="gpt-3.5-turbo", temperature=0)

    rag_chain = (
        {"context": retriever, "question": RunnablePassthrough()}
        | prompt
        | llm
        | StrOutputParser()
    )

    return rag_chain


def get_mock_chain():
    return RunnablePassthrough() | RunnablePassthrough() | StrOutputParser()


if __name__ == "__main__":
    args = parse_args()

    load_dotenv(args.dotenv_path)

    rag_chain = get_rag_chain(device=args.device, data_path=args.data_path)
    print("Type 'exit' to exit")
    while prompt := input(">>> "):
        if prompt == "exit":
            break
        print(rag_chain.invoke(prompt))
