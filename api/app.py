import argparse

from flask import Flask, request, jsonify, Response
from flask_cors import CORS
from dotenv import load_dotenv

from llm import get_rag_chain, get_mock_chain

class AppRunner:
    MAX_PROMPT_LENGTH = 1000
    MIN_PROMPT_LENGTH = 1
    
    def __init__(self, args):
        self.chain = get_mock_chain() if args.debug else get_rag_chain()
        self.app = Flask(__name__)
        cors = CORS(self.app)
        self.app.config["CORS_HEADERS"] = "Content-Type"


        @self.app.route("/message", methods=["POST"])
        def message():
            prompt = request.json["prompt"]
            prompt = prompt[:self.MAX_PROMPT_LENGTH]
            if len(prompt) < self.MIN_PROMPT_LENGTH:
                return Response(status=400)
        
            response = self.chain.invoke(prompt)
            return jsonify({"text": response})
        
    def run(self, host, port, debug):
        self.app.run(host=host, port=port, debug=debug)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", type=str, default="0.0.0.0")
    parser.add_argument("--port", type=int, default=5000)
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--dotenv_path", type=str, default=".env")
    return parser.parse_args()


def main(args):
    load_dotenv(args.dotenv_path)
    runner = AppRunner(args)
    runner.run(args.host, args.port, args.debug)
    

if __name__ == "__main__":
    args = parse_args()
    main(args)
