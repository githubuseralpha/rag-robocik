import os
import io
import re
import pathlib
import argparse
import datetime
import json

import mammoth
import fitz
from markdownify import markdownify
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, default="config.json")
    parser.add_argument("--credentials", type=str, default="credentials.json")
    parser.add_argument("--output_folder", type=str, default="data")
    parser.add_argument("--max_days", type=int, default=365)
    return parser.parse_args()


class DocumentParser:
    def __init__(self, args):
        credentials_path = args.credentials
        credentials = service_account.Credentials.from_service_account_file(
            credentials_path, scopes=["https://www.googleapis.com/auth/drive"]
        )

        self.drive_service = build("drive", "v3", credentials=credentials)

    def list_files(self, folder_id):
        results = (
            self.drive_service.files()
            .list(
                q=f"'{folder_id}' in parents",
                fields="files(id, name, mimeType, modifiedTime)",
            )
            .execute()
        )
        files = results.get("files", [])
        return files

    def clean_path(self, path):
        # Define a regular expression pattern to match illegal characters
        illegal_chars = r'[:*?"<>|\/]'

        # Use re.sub to replace illegal characters with an empty string
        cleaned_path = re.sub(illegal_chars, "", path)

        return cleaned_path

    def download_file(self, file_id, destination_folder, name, mime_type):
        try:
            if mime_type.startswith("application/vnd.google-apps."):
                request = self.drive_service.files().export_media(
                    fileId=file_id, mimeType="application/pdf"
                )
                path = os.path.join(destination_folder, f"{name}.pdf")
                path = self.clean_path(path)
                fh = io.FileIO(path, "wb")
                downloader = MediaIoBaseDownload(fh, request)

                done = False
                while done is False:
                    status, done = downloader.next_chunk()
            else:
                request = self.drive_service.files().get_media(fileId=file_id)
                path = os.path.join(destination_folder, f"{name}")
                path = self.clean_path(path)

                fh = io.FileIO(path, "wb")
                downloader = MediaIoBaseDownload(fh, request)

                done = False
                while done is False:
                    status, done = downloader.next_chunk()
        except Exception as e:
            print(f"An error occurred: {e}")

    def download_docs(
        self, folder_id, destination_folder, current_folder_name, max_days=180
    ):
        file_types = (
            "application/vnd.google-apps.document",
            "application/pdf",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        )
        files = self.list_files(folder_id)
        for file in files:
            mod_time = file["modifiedTime"]
            mod_time = datetime.datetime.strptime(mod_time, "%Y-%m-%dT%H:%M:%S.%fZ")
            current_time = datetime.datetime.now()
            if (
                file["mimeType"] in file_types
                and (current_time - mod_time).days < max_days
            ):
                print(f"Downloading {file['name']}")
                filename = f"{current_folder_name}__{file['name']}"
                self.download_file(
                    file["id"], destination_folder, filename, file["mimeType"]
                )

    def get_folders(self, folder_id):
        ignore_folders = ["Xavier"]
        results = (
            self.drive_service.files()
            .list(
                q=f"'{folder_id}' in parents",
                fields="files(id, name, mimeType, modifiedTime)",
            )
            .execute()
        )

        folders = results.get("files", [])
        folders = [
            folder
            for folder in folders
            if folder["mimeType"] == "application/vnd.google-apps.folder"
        ]
        folders = [folder for folder in folders if folder["name"] not in ignore_folders]
        return folders

    def recursive_download(
        self, folder_id, destination_folder, current_folder_name="", max_days=180
    ):
        print(f"Downloading files from folder {current_folder_name}")
        self.download_docs(
            folder_id, destination_folder, current_folder_name, max_days=max_days
        )
        folders = self.get_folders(folder_id)
        for folder in folders:
            id = folder["id"]
            name = folder["name"]
            self.recursive_download(
                id,
                destination_folder,
                f"{current_folder_name}__{name}",
                max_days=max_days,
            )

    def convert_to_md(self, file_path):
        try:
            path = pathlib.Path(file_path)
            if path.suffix == ".docx":
                with open(file_path, "rb") as docx_file:
                    result = mammoth.convert_to_markdown(docx_file)
                    markdown = result.value
                    messages = result.messages
                return markdown
            elif path.suffix == ".pdf":
                doc = fitz.open(file_path)
                markdown_content = ""
                for page_number in range(doc.page_count):
                    page = doc[page_number]
                    text = page.get_text()
                    markdown_content += text

                doc.close()
                return markdownify(markdown_content)
            else:
                print(f"Unknown file type {path.suffix}")
                return None
        except Exception as e:
            print(f"An error occurred: {e}")
            return None

    def run(self, args):
        out_path = pathlib.Path(args.output_folder)
        RAW_FILES_FOLDER = out_path / "raw"
        MARKDOWN_FILES_FOLDER = out_path / "markdown"

        os.makedirs(RAW_FILES_FOLDER, exist_ok=True)
        os.makedirs(MARKDOWN_FILES_FOLDER, exist_ok=True)

        with open(args.config, "r") as f:
            config = json.load(f)
            folders = config["folders"]

        for name, folder in folders.items():
            self.recursive_download(
                folder, RAW_FILES_FOLDER, name, max_days=args.max_days
            )


if __name__ == "__main__":
    args = parse_args()
    doc_parser = DocumentParser(args)
    doc_parser.run(args)
