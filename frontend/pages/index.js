import 'bootstrap/dist/css/bootstrap.min.css';
import Head from 'next/head';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {Col, Row} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Image from 'react-bootstrap/Image';
import Card from 'react-bootstrap/Card';
import { useState } from 'react';

export default function Home() {
  const [message, setMessage] = useState("");
  const [response, setResponse] = useState("Hello! I'm Robocik. What do you want to know?");

  const ENDPOINT = "http://localhost:5000/message";
  const GENERATION_SPEED = 3;

  async function smoothText(text, scrollable) {
    setResponse("");
    for (let char of text) {
      setResponse(prev => prev + char);
      scrollable.scrollTop = scrollable.scrollHeight;
      await new Promise(r => setTimeout(r, GENERATION_SPEED));
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const msg = message;
    setMessage("");
    fetch(ENDPOINT, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ prompt: msg }),
    })
      .then((response) => response.json())
      .then((data) => {
        smoothText(data.text, document.getElementsByClassName('card-body')[0]);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }

  return (
    <div style={{ height: "100vh", width: "100vw", backgroundColor: "#292929"}}>
      <Head>
        <title>Robocik Chat</title>
      </Head>
      <Container  className="d-flex flex-column min-vh-100">
        <Row className='mb-auto mt-5'>
          <Col lg={1} xs={0} className='d-none d-lg-block'>
            <Image src="robocik.png" roundedCircle style={{width: "12vh", height: "12vh"}}/>
          </Col>
          <Col lg={10} xs={9}>
            <Card style={{backgroundColor: "#232323", color: "white", borderColor: "white", borderRadius: "15px", maxHeight: "60vh"}}>
              <Card.Body style={{ overflowY: 'auto' }} className='mb-4 mx-3 mt-2'>
                {response}
              </Card.Body>
            </Card>
          </Col>
          <Col lg={1} xs={2}></Col>
        </Row>
        <Row className='mt-auto mb-4'>
            <Col lg={1} xs={0}></Col>
            <Col lg={10} xs={9}>
              <Form id='form' onSubmit={handleSubmit}>
                <Form.Control 
                  type="text" 
                  placeholder="Enter your name" 
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                  style={{backgroundColor: "#232323", color: "white", borderRadius: "15px", padding: "20px", fontSize: "20px"}} 
                  className='p-3'/>
              </Form>
            </Col>
            <Col lg={1} xs={2}>
                <Button variant="outline-light" 
                        type="submit" 
                        className='p-3' 
                        style={{borderRadius: "15px", fontSize: "20px"}}
                        form='form'>
                    Send
                </Button>
            </Col>
        </Row>
      </Container>
    </div>
  );
}
